package com.demo.lab.product.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class ProductDto {
	private String name;
	private String description;
	private Long price;
    private String category;
}
